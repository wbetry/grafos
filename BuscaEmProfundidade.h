#ifndef BUSCA_PROFUNDIDADE_H
#define BUSCA_PROFUNDIDADE_H

#include <stdio.h>
#include <stdlib.h>

static int cont;
int pre[1000];

struct grafo {
	int vertices; 						// número de vértices.
	int arestas; 						// número de arestas.
	int **adj; 							// ponteiro para a matriz de adjacências do grafo.
	int total;							// total de arestas.
};

typedef struct grafo *Grafo;			// ponteiro para um grafo

Grafo inicializar_grafo(int vertices);
static int **inicializar_matriz(int linhas , int colunas, int valor);
void adicionarAresta(Grafo g, int origem, int destino);
void imprimirGrafo(Grafo g);
void buscaProfundidade(Grafo G);
static void dfs(Grafo G, int v);
void liberar_grafo(Grafo g);

#endif
