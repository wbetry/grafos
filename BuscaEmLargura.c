#include "BuscaEmLargura.h"
#include <stdio.h>

int main(int argc, char const *argv[]) {
	int v = 8;

    Grafo grafo = inicializar_grafo(v);

	// adicionando as arestas
	adicionarAresta(grafo, 0, 1);
	adicionarAresta(grafo, 0, 2);
	adicionarAresta(grafo, 1, 3);
	adicionarAresta(grafo, 1, 4);
	adicionarAresta(grafo, 2, 5);
	adicionarAresta(grafo, 2, 6);
	adicionarAresta(grafo, 6, 7);

	imprimirGrafo(grafo);

	bfs(grafo, 0);

	int i;

	// variavel num se encontra em BuscaEmLargura.h
	for(i = 0; i < 100; i++) {
		printf("%d ", num[i]);
	}

	destruir_grafo(grafo);

	return 0;
}

Grafo inicializar_grafo(int vertices) {
	Grafo g = (Grafo) malloc(sizeof *g);
	g->vertices = vertices;
	g->arestas = 0;
	g->adj = (link *) malloc(vertices * sizeof (link));

	int v;
	for (v = 0; v < vertices; ++v) {
		g->adj[v] = NULL;
	}

	return g;
}

static link novo_no(int w, link proximo) {
	link a = (link) malloc(sizeof (struct node_grafo));
	a->dado = w;
	a->proximo = proximo;

	return a;
}

void adicionarAresta(Grafo g, int origem, int destino) {
	link a;
	for (a = g->adj[origem]; a != NULL; a = a->proximo) {
		if (a->dado == destino) {
			return;
		}
	}
	g->adj[origem] = novo_no(destino, g->adj[origem]);
	g->arestas++;
}

void imprimirGrafo(Grafo g) {
	int i;

	for (i = 0; i < g->vertices; i++) {
		link a;
		printf("Vertice %d: ", i);
		for (a = g->adj[i]; a != NULL; a = a->proximo) {
			printf("%d, ", a->dado);
		}
		printf("\n");
	}
}

void bfs(Grafo g, int inicio)
{
	int cont = 0;
	int vertice;
	for (vertice = 0; vertice < g->vertices; ++vertice) {
		num[vertice] = -1;
	}

	num[inicio] = cont++;

	fila *fila = inicializar_fila();
	fila = adicionar_fila(fila, inicio);

	while (!empty_fila(fila)) {
		int v = retirar_fila(fila);
		link a;
		for (a = g->adj[v]; a != NULL; a = a->proximo) {
			if (num[a->dado] == -1) {
				num[a->dado] = cont++;
				fila = adicionar_fila(fila, a->dado);
			}
		}
	}
	destruir_fila(fila);
}

void destruir_grafo(Grafo g) {
	if (g == NULL) {
		return;
	}

	int i;

	for (i = 0; i < g->vertices; ++i) {
		free(g->adj[i]);
	}

	free(g->adj);
	g->adj = NULL;
	free(g);
	g = NULL;
}
