#include "fila.h"

int main(int argc, char const *argv[])
{
	fila *fila = inicializar_fila();

	int i;

	for (i = 0; i < 10; i++) {
		adicionar_fila(fila, i);
	}

	int size = tamanho_fila(fila);

	for (i = 0; i < size; i++) {
		printf("%d ", retirar_fila(fila));
	}

	destruir_fila(fila);

	return 0;
}
