#ifndef FILA_H
#define FILA_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct node {
  	int dado;
  	struct node *proximo;
};

struct fila {
  	struct node *inicio;
  	struct node *fim;
  	unsigned int tamanho;
};

typedef struct node node;
typedef struct fila fila;

fila *inicializar_fila(void);

int tamanho_fila(fila *_fila);

int empty_fila(fila *_fila);

fila *adicionar_fila(fila *_fila, int dado);

int inicio_fila(fila *_fila);

int retirar_fila(fila *_fila);

void destruir_fila(fila *_fila);

#endif
